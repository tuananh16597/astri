/**
 * Main Javascript.
 * This file is for who want to make this theme as a new parent theme and you are ready to code your js here.
 */

jQuery(function(){
	jQuery('.shuffle-wrapper').waitForImages(function() {
		jQuery('.shuffle-wrapper').shuffle({
			speed: 250,
			itemSelector: '.shuffle-item',
		});


		jQuery('.tagfilter-item.c1').click();
	});
	jQuery('.tagfilter-item.c1').click();

	jQuery('.navbar .dropdown > a').click(function(){
		location.href = this.href;
	});

	jQuery('.slider-wrapper').waitForImages(function() {
		resize_banner();
	});


	resize_ways_of_collaboration();
	resize_milestone();
	jQuery(window).resize(function() {
		resize_banner();
		resize_ways_of_collaboration();
		resize_milestone();
	});


	// jQuery(window).scroll(function() {
	// 	scroll_function();
	// });

	// window.addEventListener('scroll', function(e) {
		// alert('test');
	// });

	// jQuery('html, body').scroll(function() {
		// scroll_function();
	// });

	// function scroll_function() {
	// 	if (shuffle_paging) {
	// 		var s = jQuery('body').scrollTop();
	// 		if (s == 0)
	// 			s = jQuery('html').scrollTop();
	// 		var h = jQuery('body').height();
	// 		var h_footer = jQuery('#site-footer').height();
	// 		var h_window = jQuery(window).height();
	//
	// 		var r2 = h - h_window - h_footer - 1500;
	// 		var r1 = h - h_window - h_footer - 500;
	//
	// 		// jQuery('#test_r').html('r:'+r);
	// 		// jQuery('#test_s').html('s:'+s);
	//
	// 		if (s > r2 && (typeof shuffle_paging_process2 !== "undefined") && !shuffle_paging_process2) {
	// 			shuffle_paging_process2 = true;
	// 			jQuery.ajax({
	// 				url: '/wp-admin/admin-ajax.php',
	// 				method: 'POST',
	// 				dataType: 'JSON',
	// 				data: {
	// 					action: 'get_more_news_general_boxes',
	// 					search_params: search_params,
	// 					posts_per_page: shuffle_paging_show,
	// 					offset: template_data.length - page_standard_boxes_num,
	// 					_records_per_page: _records_per_page,
	// 				},
	// 				success: function(r){
	//
	// 					if(r.success)
	// 					{
	// 						// console.log(r.data.request, r.data.language);
	// 						for (i = 0; i < r.data.boxes_get.length; i++) {
	// 							template_data.push(r.data.boxes_get[i]);
	// 						}
	// 						shuffle_paging_process2 = false;
	// 					}
	//
	// 				},
	// 				error: function(e){
	// 					//console.log('e', e);
	// 				},
	// 			});
	// 		}
	//
	// 		if (s > r1 && !shuffle_paging_process) {
	// 			shuffle_paging_process = true;
	//
	// 			if (shuffle_paging_template)
	// 				show_page_active_template(shuffle_paging_show);
	// 			else
	// 				show_page_active();
	// 		}
	// 	}
	// }


	banner_int = setInterval("banner_sch()", 5000);
	banner_max = jQuery(".slider-wrapper-widther .slider-item").length;
	jQuery(".slider-lists .slider-list:nth-child(" + banner_current + ")").addClass('active');
	jQuery('.active .slider-list-counter').animate({'width':'100%'}, 5000, function() {
		jQuery(this).css({'width':'0%'});
	});

	jQuery('.slider-desc-container.banner1 .slider-desc-counter').animate({'width':'100%'}, 5000, function() {
		jQuery(this).css({'width':'0%'});
	});

	jQuery(".slider-list").click(function() {
		banner_move(jQuery(this).data('id'));
	});

	jQuery(".tagfilter-item").click(function() {
		jQuery(this).parent().find(".tagfilter-item").removeClass('active');
		jQuery(this).addClass('active');

		var temp = jQuery(this).data('dsort');

		jQuery('.shuffle-wrapper').shuffle('shuffle', temp);

		sort_options = {
			'by': function(el) {
				return el.data(temp);
			}
		};
		sort_int = setInterval("shuffle_sort()", 500);
	});

	jQuery("#fs_a_1").click(function() {
		jQuery('body').css('font-size', '100%');
	});

	jQuery("#fs_a_2").click(function() {
		jQuery('body').css('font-size', '110%');
	});

	jQuery("#fs_a_3").click(function() {
		jQuery('body').css('font-size', '120%');
	});

	jQuery(".no-translation a").attr('href', '#');

	jQuery(".breadcrumbs a.home").attr('href', jQuery(".site-title-heading a").attr('href'));
});

function shuffle_sort() {
	jQuery('.shuffle-wrapper').shuffle('sort', sort_options);
	clearInterval(sort_int);
}

function show_page_active() {
	var show_active = 0;
	shuffle_paging_process = true;
	jQuery('.shuffle-item.shuffle-hidden').each(function() {
		if (show_active < shuffle_paging_show) {
			jQuery(this).removeClass('shuffle-hidden');

			jQuery(this).find('.odf_lazy').each(function() {
				jQuery(this).attr('src', jQuery(this).data('src'));
			});

			show_active++;
		}
	});

	jQuery('.shuffle-wrapper').shuffle('shuffle');

	sort_int = setInterval("shuffle_paging_done()", 500);
}

function shuffle_paging_done() {
	jQuery('.shuffle-wrapper').shuffle('shuffle');
	shuffle_paging_process = false;
	clearInterval(sort_int);
}

function resize_banner() {
	//jQuery('.slider-item').css('width', jQuery(window).width());
	//jQuery(".slider-wrapper-widther").stop(true,true).css({'margin-left': (banner_current-1) * jQuery(window).width() * -1});
	jQuery('.slider-item').css('width', jQuery('.slider-wrapper').width());
	jQuery(".slider-wrapper-widther").stop(true,true).css({'margin-left': (banner_current-1) * jQuery('.slider-wrapper').width() * -1});

	jQuery(".slider-desc-container").each(function() {
		h = (parseInt(jQuery(".slider-wrapper").css('height')) - parseInt(jQuery(this).css('height')) - 80) / 2;
		jQuery(this).css('top', h);
	});

}

function resize_ways_of_collaboration() {
	var max_height = 0;

	jQuery('.ways-of-collaboration-item .padding-block').css('height','auto');
	jQuery('.ways-of-collaboration-item .padding-block.no_border img').css('max-height','initial');
	if (jQuery(window).width() > 991) {
		jQuery('.ways-of-collaboration-item .padding-block.have_border').each(function() {
			var total_height = jQuery(this).height() + parseInt(jQuery(this).css('padding-top')) + parseInt(jQuery(this).css('padding-bottom'));
			if (total_height > max_height)
				max_height = total_height;
		});

		if (max_height) {
			jQuery('.ways-of-collaboration-item .padding-block').css('height',max_height);
			jQuery('.ways-of-collaboration-item .padding-block.no_border img').css('max-height',max_height);
		}
	} else {
		jQuery('.ways-of-collaboration-item .padding-block').css('height','auto');
		jQuery('.ways-of-collaboration-item .padding-block.no_border img').css('max-height','initial');
	}
}

function resize_milestone() {
	jQuery('.section-milestone-item').each(function() {
		var max_height = 0;

		if (jQuery(window).width() > 991) {
			jQuery(this).find('.border-box').css('height','auto');
			jQuery(this).find('.border-box').each(function() {
				var total_height = jQuery(this).height() + parseInt(jQuery(this).css('padding-top')) + parseInt(jQuery(this).css('padding-bottom'));
				if (total_height > max_height)
					max_height = total_height;
			});

			if (max_height) {
				jQuery(this).find('.border-box').css('height',max_height);
			}
		} else {
			jQuery(this).find('.border-box').css('height','auto');
		}
	});
}

var banner_current = 1;
var banner_max = 1;
var banner_int;
function banner_sch() {
	if (banner_current + 1 <= banner_max)
		banner_current++;
	else
		banner_current = 1;

	banner_move(banner_current);
}

function banner_move(i) {
	if (i == banner_max) {
		jQuery(".slider-wrapper-widther").stop(true,true).animate({'margin-left': (i-1) * jQuery('.slider-wrapper').width() * -1}, 500, 'swing', function() {
			jQuery(".slider-wrapper-widther").css('margin-left', 0);
		});
		i = 1;
	} else
		jQuery(".slider-wrapper-widther").stop(true,true).animate({'margin-left': (i-1) * jQuery('.slider-wrapper').width() * -1}, 500);

	banner_current = i;
	jQuery(".slider-lists .slider-list").removeClass('active');
	jQuery(".slider-lists .slider-list:nth-child(" + i + ")").addClass('active');

	clearInterval(banner_int);

	jQuery('.slider-list-counter').stop(true,true).css({'width':'0%'});
	jQuery('.active .slider-list-counter').animate({'width':'100%'}, 5000, function() {
		jQuery(this).css({'width':'0%'});
	});

	jQuery('.slider-desc-counter').stop(true,true).css({'width':'0%'});
	jQuery('.slider-desc-container.banner'+i+' .slider-desc-counter').animate({'width':'100%'}, 5000, function() {
		jQuery(this).css({'width':'0%'});
	});


	banner_int = setInterval("banner_sch()", 5000);
}


var mobile_menu = function($){
	// const top_menu_a = $('#menu-header a');
	// const dropdown_lists = $('#menu-header .dropdown-menu');
	const top_menu_a = $('header .nav.navbar-nav a');
	const dropdown_lists = $('header .nav.navbar-nav .dropdown-menu');

	// ADD NEXT BUTTON TO EACH A
	for (var i = 0; i < top_menu_a.length; i++) {
		$(top_menu_a[i]).html($('<span></span>').text($(top_menu_a[i]).text()));
	}
	top_menu_a.append($('<span class="menu-next"></span>'));

	// ADD BACK LI TO SUB-MENU
	for (var i = dropdown_lists.length - 1; i >= 0; i--) {
		$(dropdown_lists[i]).prepend($('<li class="menu-back"><span>' + ($(dropdown_lists[i]).prev().text() || '') + '</span></li>'));
	};

	// TOGGLE MENU FOR MOBILE
	$(document).on('click', '.mobile-nav-btn', function(){
		if ($('.mb_lang_selected').hasClass('active')) {
			$('.mb_lang_selected').removeClass('active');
		}
		$(this).hasClass('active') ? $(this).removeClass('active') : $(this).addClass('active');
	});

	// ON NEXT BUTTON CLICK (GO TO SUBMENU / JUMP INTO PAGE)
	$('.menu-next').on('click', function(){
		const _a = $(this).parent();
		const submenu = _a.next().hasClass('sub-menu');

		if (submenu) {
			// EXPAND CURRENT SUB-MENU
			_a.parent().addClass('mb_active');

			// HIDDEN OTHERS <li>
			_a.parent().siblings('li').addClass('mb_hide');

			// NO JUMPING
			return false;
		}
	});

	// ON BACK BUTTON CLICK (BACK TO PREVIOUS LEVEL)
	$('.menu-back').on('click', function(){
		const _parent_li = $(this).parent().parent();
		_parent_li.removeClass('mb_active').siblings('li').removeClass('mb_hide');
	});

	// ON LANG BUTTON CLICKED
	$('.mb_lang_selected').on('click', function(){
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
			$('.mobile-nav-btn').removeClass('active');
		} else {
			$(this).addClass('active');
			$('.mobile-nav-btn').removeClass('active');
		}
	});

	// SET UP MOBILE LANG BUTTON DISPLAY
	function init_mb_lang_display()
	{
		const _display = $('header .mb_lang_selected');
		const _lang_title = $('header a[lang=' + _display.data('lang') + ']').text() || '';
		_display.text(_lang_title);
	};
	init_mb_lang_display();

	// ON CLICK MOBILE LANG BUTTON
	$('.mb_lang').on('click', function(){
		const _href = $('header a[lang=' + $(this).data('lang') + ']').attr('href') || '';
		if (_href) {
			window.location.href = _href;
		}
		return false;
	});

}(jQuery);

var footer = function($){

	function init_footer_links()
	{
		var _links = $('.footer-links a');
		const _mb_links_wrapper = $('.mb-footer-links');

		const _table = $('<table></table>');

		var _row = $('<tr></tr>');

		for (var i = _links.length - 1; i >= 0; i--) {
			_row.prepend($('<td></td>').append($(_links[i]).clone()));
			if (_row.children().length >= 3) {
				_table.append(_row);
				_row = $('<tr></tr>');
			}
		}

		_mb_links_wrapper.append(_table);

	}

	init_footer_links();

}(jQuery);

jQuery(function($) {

	var page_language = $('html').attr('lang');
	if (page_language == "en-US") {
		var backbtntext = 'Back';
	}else{
		var backbtntext = '返回';
	}
	// alert(backbtntext);

	if($('.page-id-1181497').length == 0){
		if(($('.page-id-1100124').length == 1) || ($('.page-id-1100125').length == 1) || ($('.page-id-1100126').length == 1)){
			var node = $('<span style="float: right;" class="btn_back"><br/><input type="button" class="btn_back" value="&larr; ' + backbtntext + '" onClick="history.go(-1)"></span>');
		} else {
			var node = $('<span style="float: right;"><input type="button" class="btn_back" value="&larr; ' + backbtntext + '" onClick="history.go(-1)"></span>');
		}
		if($('.btn_back').length == 0)
			$('.breadcrumbs').append(node);
	}
});
var main_lok = function($){

	jQuery(document).on('click', '.entry-print', function(){
		window.print();
	});

	var search_bar_timeout;
	jQuery(document).on('click', '.search-form-btn', function(){
		jQuery('.search-form').addClass('active');
		jQuery('.search-form #form-search-input').focus();
	});
	jQuery(document).on('blur', '.search-form #form-search-input', function(){
		search_bar_timeout = setTimeout(function(){
			jQuery('.search-form').removeClass('active');
		}, 1000);
	});
	jQuery(document).on('focus click', '.search-form #form-search-input', function(){
		clearTimeout(search_bar_timeout);
	});
}
main_lok();
